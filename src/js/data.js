export const sendingWays = [
    {
      id: 1,
      title: 'Фельдсвязь по Москве',
    },
    {
      id: 2,
      title: 'Фельдсвязь с другими городами',
    },
    {
      id: 3,
      title: 'Через отделение почтовой связи',
    },
    {
      id: 4,
      title: 'На руки',
    }
];

export const sendingDirections = [
  {
      id: 1,
      title: 'Москва',
  },
  {
      id: 2,
      title: 'Другими города',
  },
  {
      id: 3,
      title: 'Международное',
  },
];

export const correspondenceSenders = [
  {
      id: 1,
      title: 'Мэра Москвы',
  },
  {
      id: 2,
      title: 'Правительства Москвы',
  },
  {
      id: 3,
      title: 'Управление делами Мэра и Правительства Москвы',
  },
];

export const correspondenceTypes = [
  {
      id: 1,
      title: 'Служебная корреспонденция',
  },
  {
      id: 2,
      title: 'Ответ по обращению гражданина',
  },
  {
      id: 3,
      title: 'Поручение по обращению гражданина',
  },
  {
      id: 4,
      title: 'Распорядительный документ',
  },
  {
      id: 5,
      title: 'Поздравление',
  },
  {
      id: 6,
      title: 'Уведомление по обращению гражданина',
  },
  {
      id: 7,
      title: 'Напоминание по обращению гражданина',
  },
  {
      id: 8,
      title: 'Разное',
  },
];

export const urgency = [
  {
      id: 1,
      title: 'Обычная',
  },
  {
      id: 2,
      title: 'Срочно',
  },
  {
      id: 3,
      title: 'Весьма срочно',
  },
  {
      id: 4,
      title: 'Вручить немедленно',
  },
];

export const documentTypes = [
  {
      id: 1,
      title: 'Письмо',
  },
  {
      id: 2,
      title: 'Правительственная телеграмма',
  },
  {
      id: 3,
      title: 'Приглашение',
  },
  {
      id: 4,
      title: 'Реклама',
  },
  {
      id: 5,
      title: 'Судебная повестка',
  },
  {
      id: 6,
      title: 'Телеграмма',
  },
  {
      id: 7,
      title: 'Факс',
  },
  {
      id: 8,
      title: 'Электронная почта',
  },
];

export const categories = [
  {
      id: 1,
      title: 'ДСП',
  },
  {
      id: 2,
      title: 'ОП',
  },
];