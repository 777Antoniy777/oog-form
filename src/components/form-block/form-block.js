import React, {useState} from 'react';
import {Form, Select, Input, InputNumber, DatePicker, Checkbox, ResetButton, SubmitButton, FormikDebug, FormItem} from 'formik-antd';
import {Formik} from "formik";
import * as Yup from 'yup';
import FieldsetBlock, {GridType} from '../fieldset-block/fieldset-block.js';
import FieldBlock, {PositionGridFour} from '../field-block/field-block';

const FormBlock = React.memo(({sendingWays, sendingDirections, correspondenceSenders, correspondenceTypes, urgency, documentTypes, categories}) => {
  const [validateOnChangeStatus, setValidateOnChangeStatus] = useState(false);

  return (
    <Formik
      initialValues={{
        sendingWay: '',
        sendingDirection: '',
        documentNumber: null,
        eventDate: '',
        correspondenceSender: correspondenceSenders[0].title,
        correspondenceType: '',
        isOther: false,
        acceptanceDate: '',
        urgency: '',
        note: '',
        correspondenceKind: '',
        copiesNumber: 0,
        category: '',
        exemplarNumber: 0,
        pageCount: 0,
      }}
      onSubmit={(values, actions) => {
        setTimeout(() => {
          console.log('submitted');

          actions.resetForm();
          actions.setSubmitting(false);

          setValidateOnChangeStatus(false);
        }, 500);
      }}
      validationSchema={Yup.object().shape({
        sendingWay: Yup.string()
          .required('required'),
        documentNumber: Yup.string()
          .nullable().default(null)
          .required('required')
          .test('number', 'value is not number', (val) => {
            if (Number.isNaN(+val)) {
              return false;
            }

            return true;
          })
          .when('sendingWay', (sendingWay, schema) => {
            return sendingWay === 'На руки' 
              ? schema.min(5, 'для "На руки" минимальная длина значения 5')
              : schema.min(3, 'минимальная длина значения 3');
          })
      })}
      validateOnChange={validateOnChangeStatus}
      validateOnBlur={false}

      // validate={values => {
      //   const errors = {};

      //   if (!values.sendingWay) {
      //     errors.sendingWay = 'required';
      //   }

      //   if (!values.documentNumber) {
      //     errors.documentNumber = 'required';
      //   }

      //   if (values.documentNumber && values.documentNumber.length < 3) {
      //     errors.documentNumber = 'min length required';
      //   }

      //   return errors;
      // }}
    >
      {(props) => {
        // console.log(props)

        const handleFieldChange = (evt) => {
          const target = evt.target;
          const name = target.name;
          const value = target.value.trim();

          props.setFieldValue(name, value, false);
        };

        const handleSubmitButtonClick = () => {
          setValidateOnChangeStatus(true);
        };

        return (
          <Form>
            <FieldsetBlock title="Информация о документе" gridType={GridType.four}>
              <FieldBlock title="Способ отправки" position={PositionGridFour.left}>
                <FormItem
                  name="sendingWay"
                >
                  <Select
                    name="sendingWay"
                    placeholder="Способ отправки"
                  >
                    {sendingWays.map((elem) => (
                      <Select.Option key={elem.id} value={elem.title}>
                        {elem.title}
                      </Select.Option>
                    ))}
                  </Select>
                </FormItem>
              </FieldBlock>
        
              <FieldBlock title="Направление отправки" position={PositionGridFour.right}>
                <Select
                  name="sendingDirection"
                  placeholder="Направление отправки"
                >
                  {sendingDirections.map((elem) => (
                    <Select.Option key={elem.id} value={elem.title}>
                      {elem.title}
                    </Select.Option>
                  ))}
                </Select>
              </FieldBlock>
        
              <FieldBlock title="№ документа" position={PositionGridFour.left}>
                <FormItem
                  name="documentNumber"
                >
                  <Input 
                    name="documentNumber"
                    maxLength={128} 
                    placeholder="№ документа"
                    onChange={handleFieldChange}
                  />
                </FormItem>
              </FieldBlock>
        
              <FieldBlock title="Дата мероприятия" position={PositionGridFour.right}>
                <DatePicker 
                  name="eventDate" 
                  placeholder="Дата мероприятия" 
                />
              </FieldBlock>
        
              <FieldBlock title="Корреспонденция от" position={PositionGridFour.left}>
                <Select
                  name="correspondenceSender"
                  placeholder="Отправитель"
                >
                  {correspondenceSenders.map((elem) => (
                    <Select.Option key={elem.id} value={elem.title}>
                      {elem.title}
                    </Select.Option>
                  ))}
                </Select>
              </FieldBlock>
        
              <FieldBlock title="Тип корреспонденции" position={PositionGridFour.right}>
                <Select 
                  name="correspondenceType"
                  placeholder="Тип корреспонденции" 
                  disabled={true} 
                >
                  {correspondenceTypes.map((elem) => (
                    <Select.Option key={elem.id} value={elem.title}>
                      {elem.title}
                    </Select.Option>
                  ))}
                </Select>
              </FieldBlock>
        
              <FieldBlock position={PositionGridFour.full}>
                <Checkbox 
                  name="isOther"
                >
                  Разное
                </Checkbox>
              </FieldBlock>
        
              <FieldBlock title="Дата приема на отправку" position={PositionGridFour.left}>
                <DatePicker 
                  name="acceptanceDate"
                  showTime={true}
                  placeholder="Дата приема на отправку" 
                  disabled={true} 
                />
              </FieldBlock>
        
              <FieldBlock title="Срочность" position={PositionGridFour.right}>
                <Select
                  name="urgency"
                  placeholder="Срочность"
                >
                  {urgency.map((elem) => (
                    <Select.Option key={elem.id} value={elem.title}>
                      {elem.title}
                    </Select.Option>
                  ))}
                </Select>
              </FieldBlock>
        
              <FieldBlock title="Примечание" position={PositionGridFour.full}>
                <Input 
                  name="note"
                  maxLength={512} 
                  placeholder="Текст примечания" 
                />
              </FieldBlock>
        
              <FieldBlock title="Вид" position={PositionGridFour.left}>
                <Select 
                  name="correspondenceKind"
                  placeholder="Письмо" 
                >
                  {documentTypes.map((elem) => (
                    <Select.Option key={elem.id} value={elem.title}>
                      {elem.title}
                    </Select.Option>
                  ))}
                </Select>
              </FieldBlock>
        
              <FieldBlock title="Количество экземпляров" position={PositionGridFour.rightHalfLeft}>
                <InputNumber 
                  name="copiesNumber" 
                  step={1} 
                  min={1}
                />
              </FieldBlock>
        
              <FieldBlock title="Категория" position={PositionGridFour.left}>
                <Select 
                  name="category"
                  placeholder="Категория" 
                  disabled={true} 
                >
                  {categories.map((elem) => (
                    <Select.Option key={elem.id} value={elem.title}>
                      {elem.title}
                    </Select.Option>
                  ))}
                </Select>
              </FieldBlock>
        
              <FieldBlock title="Экземпляр №" position={PositionGridFour.rightHalfLeft}>
                <InputNumber 
                  name="exemplarNumber" 
                  step={1} 
                  min={1} 
                />
              </FieldBlock>
        
              <FieldBlock title="Количество листов" position={PositionGridFour.rightHalfRight}>
                <InputNumber 
                  name="pageCount" 
                  step={1} 
                  min={1} 
                />
              </FieldBlock>
            </FieldsetBlock>
        
            <div className="button-block" style={{textAlign: 'center'}}>
              <ResetButton>Reset</ResetButton>
      
              <SubmitButton  
                type="primary" 
                disabled={false}
                onClick={handleSubmitButtonClick}
              >
                Submit
              </SubmitButton>
            </div>

            <FormikDebug style={{ maxWidth: 400 }} />
          </Form>
        )
      }}
    </Formik>
  );
});

export default FormBlock;
