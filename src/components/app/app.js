import {sendingWays, sendingDirections, correspondenceSenders, correspondenceTypes, urgency, documentTypes, categories} from "../../js/data";
import FormBlock from "../form-block/form-block";

const App = () => {
  return (
    <FormBlock
      sendingWays={sendingWays}
      sendingDirections={sendingDirections}
      correspondenceSenders={correspondenceSenders}
      correspondenceTypes={correspondenceTypes}
      urgency={urgency}
      documentTypes={documentTypes}
      categories={categories}
    />
  );
}

export default App;
