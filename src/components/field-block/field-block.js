import React from 'react';
import '../../styles/field-block.scss';

export const PositionGridFour = {
  left: 'left',
  right: 'right',
  leftHalfLeft: 'leftHalfLeft',
  leftHalfRight: 'leftHalfRight',
  rightHalfLeft: 'rightHalfLeft',
  rightHalfRight: 'rightHalfRight',
  full: 'full',
  threeFour: 'threeFour',
  threeFourRight: 'threeFourRight',
}

export const PositionGridThree = {
  left: 'left',
  right: 'right',
  center: 'center',
  full: 'full',
}

export const PositionGridSix = {
  left: 'left',
  right: 'right',
  middle: 'middle',
  leftLeft: 'leftLeft',
  leftRight: 'leftRight',
  leftRightRight: 'leftRightRight',
  leftMiddle: 'leftMiddle',
  rightLeft: 'rightLeft',
  rightRight: 'rightRight',
  rightMiddle: 'rightMiddle',
  threeLeft: 'threeLeft',
  threeMiddle: 'threeMiddle',
  threeRight: 'threeRight',
  full: 'full',
  fiveSix: 'fiveSix',
  leftMiddleRight: 'leftMiddleRight',
}

export function getClassNameGrid(position) {
  return position || 'no-position';
}

const FieldBlock = React.memo(
  ({ title = '', children, position, classSpecial, customHeader, actions, style }) => {
    const positionClass = getClassNameGrid(position);

    return (
      <div className={`${positionClass} ${classSpecial} field-block`} style={style}>
        <div className="field">
          <div className="field-block_title">
            {customHeader ?? <span>{title}</span>}
            {actions && <div>{actions}</div>}
          </div>
          {title === null && <br />}
        </div>
        {children}
      </div>
    );
  }
);

export default FieldBlock;
