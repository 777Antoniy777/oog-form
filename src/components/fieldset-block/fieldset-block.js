import React from 'react';
import '../../styles/fieldset-block.scss';

export const GridType = {
  one: 'one',
  three: 'three',
  four: 'four',
  six: 'six',
}

const FieldsetBlock = React.memo(({ title, children, gridType }) => {
  return (
    <fieldset className="fieldset-large">
      <legend className="legend-h1">{title}</legend>
      <div className={`grid-${gridType}`}>{children}</div>
    </fieldset>
  );
});

export default FieldsetBlock;